package fr.unice.miage.batmoyage;

import java.io.IOException;
import java.net.*;

public class Server {

    public static void main(String[] zero){

        ServerSocket socket;
        try {
            socket = new ServerSocket(2009);
            Thread t = new Thread(new Accepter_joueurs(socket));
            t.start();
            System.out.println("Les joueurs sont prêts !");

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}

class Accepter_joueurs implements Runnable {

    private ServerSocket socketserver;
    private Socket socket;
    private int nbrjoueur = 1;
    public Accepter_joueurs(ServerSocket s){
        socketserver = s;
    }

    public void run() {

        try {
            while(true){
                socket = socketserver.accept(); // Un client se connecte on l'accepte
                System.out.println("Le joueur numéro "+nbrjoueur+ " est connecté !");
                nbrjoueur++;
                socket.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
